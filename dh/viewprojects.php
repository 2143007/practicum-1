<?php
session_start();

	include("connection.php");
	include("validate.php");
    include("userdetails.php");

	$user_data = check_login($con);
    $user_details = getUserDetails($con);

?>

<html>
    <title>View Projects</title>
    <head><link rel="stylesheet" href="css/backup.css"></head>
    <a href="index.php">Home</a>
    <body>
    <table class="container">
					<thead>
						<tr>
							<th>Project Name</th>
							<th>Control Number</th>
							<th>Abstract</th>
							<th>Adviser</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$query = mysqli_query($con, "SELECT * FROM `project`") or die(mysqli_error());
							while($fetch = mysqli_fetch_array($query)){
						?>
						<tr>
							<td><?php echo $fetch['project_name']?></td>
							<td><?php echo $fetch['control_number']?></td>
                            <td><?php echo $fetch['abstract']?></td>
                            <td><?php echo $fetch['adviser']?></td>
							<td><a href="projectview.php?project_id=<?php echo $fetch['project_id']?>"> Open Project</a></td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
        </body>
</html>