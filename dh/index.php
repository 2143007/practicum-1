<?php
session_start();

	include("connection.php");
	include("validate.php");
    include("userdetails.php");

	$user_data = check_login($con);
    $user_details = getUserDetails($con);

?>

<!DOCTYPE html>
<html>

    <head>
        <title>Home page - Department Head</title>
        <link rel="stylesheet" href="css/backup.css">
    </head>

    <body>


        <h1>Department Head</h1>
        <h2>Welcome <?php echo $user_details['first_name'];?>!</h2>
        <!--<br>
        <h2>Hello, <?php echo $user_data['school_id']; ?></h2>
        <br>
        <h2>Your first name is <?php echo $user_details['first_name'];?></h2>
        -->


        <a href="viewprojects.php">View Projects</a>
        <a href="newproject.php">Add New Project</a>
        <a href="logout.php">Logout</a>
    </body>
</html>