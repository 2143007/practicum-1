<?php
session_start();

	include("connection.php");
	include("validate.php");
    include("userdetails.php");

	$user_data = check_login($con);
    $user_details = getUserDetails($con);

?>

<!DOCTYPE html>
<html>

    <head>
        <title>Home page - Student</title>
    </head>

    <body>

        <a href="logout.php">Logout</a>
        <h1>This is the index page</h1>

        <br>
        Hello, <?php echo $user_data['school_id']; ?>
        <br>
        Your first name is <?php echo $user_details['first_name'];?>
    </body>
</html>